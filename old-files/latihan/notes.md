# RHCSA EXAM CHEAT SHEET
## ACCESSING SYSTEMS AND OBTAINING SUPPORT
## Accessing The Command Line



```bash

# linux menyediakan 6 console. 1 physical console dan 5 virtual console
# Di RHEL8 tty1 (virtual console 1) defaultnya adalah sebuah GUI dan untuk CLI terdapat pada virtual console 2 - 6 (tty2 - tty6)

ctrl+f1 (physical console)
ctrl+f2 ke ctrl+f6 (virtual console. ex : tty1-6) 
ctrl+f7 (GUI)
#bash adalah salah satu shell yang disediakna oleh linux 
#Shell adalah sebuah interpreter (mengeksekusi command ) seperti python dll
#Shell (bash, sh, zsh)
#Shell mempunyai 3 bagian dalam penggunaannya, 
#Commmand 
ls
#Option 
ls -lah
#Argument 
ls -lah /tmp
#Terminal mengkoneksikan ke shell dari gui

```

### Changing Permissions with the Numeric Method
```bash
#change permission dengan octal number
#4 = read
#2 = write
#1 - execute
#setuid = 4; setgid = 2; sticky = 1
#4+2+1 = 7 (read,write,execute)
chmod 0777 somefile

#read in user permission
chmod 0600 somefile 

```

### - Simple or Basic Command
```bash
ls -al /tmp
# ls --> command (a program which run in /usr/bin)
# -al --> option
# /tmp --> argument
```
```bash
date
date +%R
date +%x

file /etc/passwd
cat /etc/passwd
wc /etc/passwd


head -n 3 \
/var/log/syslog \ 
/etc/passwd


#run command previous using char !
!ls
!26

# 26 is number line in .bash_history or output history command
```

## Configuring and Securing SSH 

SSH dapat digunakan dengan 3 metode
- metode user dan password
- metode private dan public key
- metode private dan public key dengan passphrase (password)


```bash

```

## Accessing System & Obtaining Support

### - Create report via CLI
```bash
sosreport
#masukkan nomer tiket (harus nomor ex: 123456) dan enter-enter
#catat checksum

#hasil report disimpan di /var/tmp/
ls -lah /var/tmp
```

### - Create report via GUI (Cockpit)
```bash
systemctl status cockpit
systemctl start cockpit
systemctl status cockpit
#kemudian akses serverb:9090 untuk simulasi report
#Masuk ke menu diagnostic and download report
```

## Navigating File Systems
```bash
# hardlink dan softlink antar direktori hanya dapat dilakukan dalam satu subdirektori yang sama
# tidak berlaku jika ingin melakukan link dari direktori /boot ke direktori /root karena beda direktori awalnya

```

## Managing Local Users & Groups
```bash
# Seperti yang telah kita ketahui, Linux termasuk sistem multiuser dimana suatu resource bisa digunakan oleh banyak user. Setiap user biasanya diberi ruangan atau space yang di simpan rapi dibawah direktori /home. Setiap user di home masing-masing memiliki hak mengakses, membaca atau menulis file-file di dalam home mereka sendiri tetapi mereka belum tentu bisa melakukan hal yang sama di home milik user lain atau direktori milik root. Masing-masing user bisa diberi hak-hak khusus yang berlainan untuk mengakses, membaca atau menulis ke sebuah file atau direktori oleh root. Oleh karena itu kita bisa saja meninggalkan root atau tidak lagi login sebagairoot sepanjang hari dengan cara membuat home sendiri, login sebagai user biasa serta memberikan hak akses seperlunya saja agar tidak membahayakan sistem bila suatu saat kita melakukan kesalahan.

# Setiap user paling sedikit bergabung dengan sebuah group. Group bisa berisi kumpulan user lain atau program yang mempunyai kesamaan tugas. Group memungkinkan sebuah file bisa dipergunakan secara bersama hanya oleh user-user yang tergabung didalamnya. Oleh karena itu cara mengelompokkan user-user dalam group ini adalah salah satu cara yang mudah bagi root untuk memberikan hak akses file-file miliknya kepada sekelompok user.

# Ada 3 bagian penting dalam linux user, superuser, system user, regular user
# regular user : UID >= 1000
# System user : UID 1 - 999
# Superuser : UID 0

uid username

# all members of the wheel group can use sudo to run commands as any user, including root
```
## Controlling Access to Files

### - Basic Permission




### - Advanced Permission
#### sticky 
 - menghindari dihapusnya sebuah file oleh userlain walaupun mempunyai permission read and write
 - format other = rwt
 - dapat diedit oleh user lain, tp tidak dapat dihapus atau di  rename oleh yang bukan ownernya

#### suid
 - 
#### sgid
 - 


## Managing SELinux Security


### - Allow selinux di file index.html

```bash
#List records of the fcontext object type
semanage fcontext -l
semanage fcontext -l | grep "httpd_sys_content_t"

#Add a record of the specified object type httpd_sys_content_t
semanage fcontext -a -t httpd_sys_content_t /var/www/html/index.html

#restore file(s) default SELinux security contexts.
restorecon -RF /var/www/html/index.html
```

## Tuning System Performance
## Monitoring & Managing Linux Processes

### Load Avarage
### CPU Utilization

## Installing and Updating Software Package
```bash
#list repo yang ada
yum repolist
```

## Managing Basic Storage
```bash


#priotiyi swap partition -1 -> 99 (ex in fstab : /dev/vdb1 swap pri=1 0 0)

```


#### REF
```bash
https://blog.cilsy.id/2015/06/seri-lvm-1-konsep-logical-volume-management-lvm-di-linux.html
```


## Controlling Services and the Boot Process
```bash


#### Boot Linux Target #####
# Normal Situation : graphical.target (GUI) & multi-user.target (CLI)
# Rescue Situation : rescue.targetemergency.target
# Emergency Situation : emergency.target
############################

#### Change defaults boot target to multi-user
systemctl get-default
systemctl set-default multi-user.targe
systemctl get-default

#### Change defaults boot target to graphical
systemctl get-default
systemctl set-default graphical.targe
systemctl get-default

#### Set target in the boot to rescue
systemd.unit=rescue.target

#### Set target in the boot to emergency
systemd.unit=emergency.target

#### Reset Password
rd.break console=tty0
mount -o remount, rw /sysroot
chroot /sysroot
passwd root
touch /.autorelabel
exit
```

## Managing Networking
## Analyzing and Storing Logs
## Implementing Advanced Storage Features
## Scheduling Future Tasks
## Accessing Network-Attached Storage
## Managing Network Securityb
## Comprehensive Review



## Free Notes
```bash
chmod 000 /usr/bin/chmod
yum provides /usr/bin/chmod
yum reinstall coreutils
```


#####
```bash
### 

```


#### Autofs
```bash

```
