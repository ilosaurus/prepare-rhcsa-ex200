===============Create and configure file systems======
### Mount and unmount network file systems using NFS
###Automounter
1. Create master map file
vim /etc/auto.master.d/shares.autofs
/remote /etc/shares

2. Create indirect map file
    *   -rw,sync,fstype=nfs4    myhostname:/shares/&


###Manage Multi Layered Storage
1. Create pool
2. 
    stratis pool create stratispool1 /dev/vdb
2. Add disk to pool
   
     stratis add-data stratispool1 /dev/vdc
3. Cek pool
    
    stratis pool List
    
    stratis blockdev list stratispool1

4. Create filesystem
    
    stratis filesystem cretae stratispool1 stratis-filesystem1
   
     stratis filesystem list


================Manage users and groups================
1. Create user with user id, comment and default shell

    useradd -u 10001 -c "Tes User" -s /bin/zsh
2. Prevent user to access shell/ to login

    useradd -s /sbin/nologin user1
3. Chan



================Manage Security========================

###Configure firewall settings using firewall-cmd/firewalld
1. Set default zone

    firewall-cmd --set-default-zone=public

2. Add source address to zone  

    firewall-cmd --permanent --zone=internal --add-source=192.168.0.0/24

3. Add service/port to zone

    firewall-cmd --permanent --zone=internal --add-service=mysql
    firewall-cmd --permanent --zone=internal --add-port=3306/tcp

4. Reload firewalld

    firewall-cmd --reload

###Configure key-based authentication for SSH
bikin pub-priv key

### Set enforcing and permissive modes for SELinux
1. Cek status SELinux

    sestatus atau getenforce
2. Set SELinux temporary

    setenforce enforcing

3. Set SELinux permanent

    Edit file--> vi /etc/selinux/config

4. Add new port 89 to httpd

    semanage port -a -t http_port_t -p tcp 89


### List and identify SELinux file and process context

### Restore default file contexts
1. List SELinux fcontext file

    ll -Z index.html

2. Cek daftar fcontext

    semanage fcontext -l

3. Relabelling fcontext

    semanage fcontext -a -t httpd_sys_content_t '/tes(/.*)?

4. Rsetore default selinux label

    restorecon -RFvv /tes

###Use boolean settings to modify system SELinux settings
1. Cek SElinux boolean List

    getsebool -a

2. Mengaktifkan boolean

    setsebool -P httpd_enable_homedirs on

###Diagnose and address routine SELinux policy violations
    less /var/log/messages
    less /var/log/audit/audit.log
    sealert -l [id-message]
    ausearch -m AVC -ts recent

==============Configure local storage==============
1. List, create, delete partitions on MBR and GPT disks

parted /dev/vdb mklabel gpt mkpart primary xfs 513M 613M

