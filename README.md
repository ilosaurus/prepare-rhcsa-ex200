# Study points RHCSA

## Manage local user and group
```bash
# Example 
# Add 3 users: harry, natasha, tom.
# The requirements: The Additional group of the two users: harry, Natasha is the admin group.
#  The user: tom's login shell should be non-interactive.
```

```bash
sudo -i

# add user and set password 
useradd harry
useradd natasha
useradd tom
passwd harry
passwd natasha
passwd tom

# set supplementary group admin to user harry & Natasha
# check if group exist
cat /etc/group | grep admin
# if group not exist, create group admin
groupadd admin

usermod -aG admin harry
usermod -aG admin natasha

cat /etc/group | grep admin



#  set shell non-interactive (no login) to user tom
usermod -s /sbin/nologin tom
cat /etc/passwd | grep tom


############################################################
# CHAPTER 3 : MANAGING LOCAL USERS AND GROUPS
# Page : 109
############################################################
```

## Tune system performance
```bash
#install tuned 
sudo -i
yum -y install 
#set tuned profile to recommend profile
tuned-adm list
tuned-adm active
tuned-adm recommend

# tuned-adm profile [PROFILE]
tuned-adm profile virtual-guest
tuned-adm active


############################################################
# CHAPTER 6 : TUNING SYSTEM PERFORMANCE
# Page : 213
############################################################
```

## Control services and the boot process
### Boot, reboot, and shut down a system normally
```bash
systemctl reboot
```
### Boot systems into different targets manually
```bash
## change defaults target to graphical
## login as root
systemctl get-default 
systemctl set-default graphical.target
reboot


## change defaults target to multi-user
## login as root
systemctl get-default 
systemctl set-default multi-user.target
reboot
``` 


### Interrupt the boot process in order to gain access to a system
```bash
## change root password
### reboot machine
### press 'e' in grub process, in kernel line add 'rd.break console=tty0' & Ctrl + x in keyboard
### in console
mount -o remount,rw /sysroot
chroot /sysroot
passwd root
touch /.autorelabel
exit
exit
### machine will reboot and booting normally with a new password

```

## ENABLING YUM SOFTWARE REPOSITORIES

## Repositories from URL
```bash
# example 
# Set local repositories from url http://classroom.example.com/repo/BaseOS & http://classroom.example.com/repo/AppStream 
```

```bash
## show cdrom device (ex: /dev/sr0)
touch /etc/yum.repos.d/rhel-local.repo 
chmod 644 /etc/yum.repos.d/rhel-local.repo 
vim /etc/yum.repos.d/rhel-local.repo 
...
    [local-BaseOS]
    name=Red Hat Enterprise Linux 8.0.0
    baseurl=http://classroom.example.com/repo/BaseOS
    enabled=1
    gpgcheck=0

    [local-AppStream]
    name=Red Hat Enterprise Linux 8.0.0
    baseurl=http://classroom.example.com/repo/AppStream 
    enabled=1
    gpgcheck=0
...

yum repolist 
```

## Repositories from CD/DVD ROM
```bash
## show cdrom device (ex: /dev/sr0)
blkid
mount /dev/sr0 /mnt/rhel8
ls -lah /mnt/rhel8/
cp /mnt/rhel8/media.repo /etc/yum.repos.d/rhel-local.repo 
chmod 644 /etc/yum.repos.d/rhel-local.repo 
vim /etc/yum.repos.d/rhel-local.repo 
...
    [local-AppStream]
    name=Red Hat Enterprise Linux 8.0.0
    mediaid=None
    baseurl=file:///mnt/rhel8/AppStream
    enabled=1
    metadata_expire=-1
    gpgcheck=0


    [local-BaseOS]
    name=Red Hat Enterprise Linux 8.0.0
    mediaid=None
    baseurl=file:///mnt/rhel8/BaseOS
    enabled=1
    metadata_expire=-1
    gpgcheck=0
...

yum repolist 
```


## Managing Basic Storage

```bash
#example
#create gpt partition 200MB (xfs) from disk /dev/vdb  and mount  to /mnt/data
lsblk 
parted -s /dev/vdb mklabel gpt
parted -s /dev/vdb mkpart primary 2048s  200MB
parted -s /dev/vdb print

lsblk | grep vdb
############### OUTPUT ###################
# vdb           252:16   0    2G  0 disk 
# `-vdb1        252:17   0  190M  0 part 
############### OUTPUT ###################

# create mount point directory 
mkdir /mnt/data

# format /dev/vdb1 to xfs 
mkfs.xfs /dev/vdb1

# copy UUID 
blkid | grep vdb1
############### OUTPUT ###################
# /dev/vdb1: UUID="b92b7ef9-dd15-41f8-acb8-b09e82ed2deb" TYPE="xfs" PARTLABEL="primary" PARTUUID="4247de56-70be-42e7-8558-dbc7ae3ce00c"
############### OUTPUT ###################

# mount using fstab (persistent)
vim /etc/fstab
    ...
    UUID="b92b7ef9-dd15-41f8-acb8-b09e82ed2deb"  /mnt/data  xfs   defaults 0 0
    ...

systemctl daemon-reload
mount -a
df -h
```


```bash
# example
# create gpt partition 500MB from disk /dev/vdb  and set to swap
lsblk | grep vdb
############### OUTPUT ###################
# vdb           252:16   0    2G  0 disk 
# `-vdb1        252:17   0  190M  0 part 
############### OUTPUT ###################

parted /dev/vdb print free
############### OUTPUT ###################
# Number  Start   End     Size    File system  Name     Flags
#        17.4kB  1049kB  1031kB  Free Space
# 1      1049kB  200MB   199MB   xfs          primary
#        200MB   2147MB  1947MB  Free Space
############### OUTPUT ###################

parted -s /dev/vdb mkpart primary 201MB 700MB
parted /dev/vdb print 

# change file system to swap
mkswap /dev/vdb2 
blkid | grep /dev/vdb

############### OUTPUT ###################
# /dev/vdb1: UUID="b92b7ef9-dd15-41f8-acb8-b09e82ed2deb" TYPE="xfs" PARTLABEL="primary" PARTUUID="4247de56-70be-42e7-8558-dbc7ae3ce00c"
# /dev/vdb2: UUID="f3ecd6c7-beed-45f4-b058-f983f25d5533" TYPE="swap" PARTLABEL="primary" PARTUUID="bf962d5b-842f-4511-a3bd-5898a8c6fb9b"
############### OUTPUT ###################

# mount using fstab (persistent)
vim /etc/fstab
    ...
    UUID="f3ecd6c7-beed-45f4-b058-f983f25d5533"  swap   swap   defaults 0 0
    ...

systemctl daemon-reload
mount -a

swapon /dev/vdb1
swapon --show

df -h
```

## Implementing Advanced Storage Feature

### Logical Volume (extend & reduce)
```bash
# example 
# create logical volume with name lv0  and filesystem xfs 500MB and mount to /mnt/data using /dev/vdc1 & /dev/vdc2
# volume group name = vg0
# /dev/vdc = 2G
# /dev/vdc1 = 1G, /dev/vdc2=1G

parted /dev/vdc/ print free
############### OUTPUT ###################
# Number  Start   End     Size    File system  Name     Flags
#         17.4kB  1049kB  1031kB  Free Space
#  1      1049kB  1024MB  1023MB               primary
#  2      1024MB  2147MB  1123MB               primary
############### OUTPUT ###################

parted -s /dev/vdc set 1 lvm on
parted -s /dev/vdc set 2 lvm on
pvcreate /dev/vdc1 /dev/vdc2
pvs
vgcreate vg0 /dev/vdc1  /dev/vdc2
vgs
lvcreate -n lv0 -L 500MB  vg0
lvs
mkfs.xfs /dev/vg0/lv0

blkid | grep lv0
############### OUTPUT ###################
# /dev/mapper/vg0-lv0: UUID="e87c49a0-d520-488a-8480-1eec919f6dbe" TYPE="xfs"
############### OUTPUT ###################

# mount using fstab (persistent)
vim /etc/fstab
    ...
    UUID="e87c49a0-d520-488a-8480-1eec919f6dbe"  /mnt/data   xfs  defaults 0 0
    ...

systemctl daemon-reload
mount -a
df -h
```

```bash
# example (xfs)
# extend existing logical volume lv0 from 500MB to 700MB and mount to /mnt/data
df -h | grep lv0
############### OUTPUT ###################
# /dev/mapper/vg0-lv0    495M   29M  466M   6% /mnt/data
############### OUTPUT ###################

lvextend /dev/vg0/lv0 -L 700MB
df -h | grep lv0
############### OUTPUT ###################
# /dev/mapper/vg0-lv0    495M   29M  466M   6% /mnt/data
############### OUTPUT ###################

xfs_growfs /mnt/data
df -h | grep lv0
############### OUTPUT ###################
# //dev/mapper/vg0-lv0    695M   31M  665M   5% /mnt/data
############### OUTPUT ###################

```


```bash
# example (ext4)
# extend existing logical volume lv1 from 500MB to 700MB and mount to /mnt/data2
blkid | grep lv1
lvs | grep lv1
df -h | grep lv1
############### OUTPUT ###################
# /dev/mapper/vg0-lv1    477M  2.3M  445M   1% /mnt/data2
############### OUTPUT ###################

umount /mnt/data2 
e2fsck -f /dev/vg0/lv1
resize2fs /dev/vg0/lv1 
mount /dev/vg0/lv1 /mnt/data2/

df -h | grep lv1
############### OUTPUT ###################
# /dev/mapper/vg0-lv1    670M  2.5M  629M   1% /mnt/data2
############### OUTPUT ###################

```

### Stratis
```bash
# example 
# create stratis pool str0 and filesystem str0fs0 from /dev/vdd
# mount str0fs0 to /mnt/stratis
# create snapshot of str0fs0 with name str0fs0-snap and mount to /mnt/stratis-snap
# /dev/vdd = 2G
yum install stratisd stratis-cli
systemctl enable --now stratisd

mkdir /mnt/stratis
/mnt/stratis-snap
stratis pool create str0 /dev/vdd
stratis blockdev list
stratis pool list

stratis filesystem create str0 str0fs0
stratis filesystem list
############### OUTPUT ###################
# Pool Name  Name     Used     Created            Device                 UUID                              
# str0       str0fs0  546 MiB  Sep 19 2019 20:09  /stratis/str0/str0fs0  f3071fa008994344a7ae9644b17bbaba  
############### OUTPUT ###################

blkid | grep f3071fa008994344a7ae9644b17bbaba
############### OUTPUT ###################
# /dev/mapper/stratis-1-8d4c51b8a1a8447d8d9102a9c331de24-thin-fs-f3071fa008994344a7ae9644b17bbaba: UUID="f3071fa0-0899-4344-a7ae-9644b17bbaba" TYPE="xfs"
############### OUTPUT ###################

vim /etc/fstab
    ...
    UUID="f3071fa0-0899-4344-a7ae-9644b17bbaba" /mnt/stratis xfs defaults,x-systemd.requires=stratisd.service 0 0
    ...

systemctl daemon-reload 
mount -a
df -h

echo "hello from stratis file" >> /mnt/stratis/file

stratis filesystem snapshot str0 str0fs0 str0fs0-snap
stratis filesystem list
############### OUTPUT ###################
# Pool Name  Name          Used     Created            Device                      UUID                              
# str0       str0fs0       546 MiB  Sep 19 2019 20:09  /stratis/str0/str0fs0       f3071fa008994344a7ae9644b17bbaba  
# str0       str0fs0-snap  546 MiB  Sep 19 2019 20:19  /stratis/str0/str0fs0-snap  8b50767b3e2444d491454a796d476f99  
############### OUTPUT ###################

blkid | grep 8b50767b3e2444d491454a796d476f99
############### OUTPUT ###################
# /dev/mapper/stratis-1-8d4c51b8a1a8447d8d9102a9c331de24-thin-fs-8b50767b3e2444d491454a796d476f99: UUID="8b50767b-3e24-44d4-9145-4a796d476f99" TYPE="xfs"
############### OUTPUT ###################

vim /etc/fstab
    ...
    UUID="8b50767b-3e24-44d4-9145-4a796d476f99" /mnt/stratis-snap xfs defaults,x-systemd.requires=stratisd.service 0 0
    ...

systemctl daemon-reload 
mount -a
df -h

cat /mnt/stratis-snap/file
```




### VDO
```bash

```

## Access network-attached storage
### NFS
```bash

# example
# serverb share nfs in directory shares (serverb:/shares) and mount to mountpoint /mnt/shares
#mount -t nfs [SERVER HOSTNAME/IP]:/SHAREDIRECTORY [MOUNTPOINT]
sudo -i
mkdir /mnt/shares
mount -t nfs serverb:/shares /mnt/shares
df -h

# make it persistent
vim /etc/fstab
    ...
    serverb:/shares  /mnt/shares    nfs     rw,sync   0 0
    ...

systemctl daemon-reload
mount -a 
df -h


############################################################
# CHAPTER 14 : ACCESSING NETWORK-ATTACHED STORAGE
# Page : 521
############################################################
```


### NFS with autofs
####  NFS autofs direct
```bash
yum -y install autofs
systemctl enable --now autofs
```
```bash
vim /etc/auto.master.d/direct.autofs
    ...
    /-   /etc/auto.direct
    ...    


vim /etc/auto.direct
    ...
    /external    -rw,sync.fstype=nfs4   host:/mount/point
    ...
```

####  NFS autofs indirect
```bash
vim /etc/auto.master.d/indirect.autofs
    ...
    /internal  /etc/auto.indirect
    ...    

vim /etc/auto.indirect
    ...
    *    -rw,sync.fstype=nfs4   host:/mount/point&
    ...
```

```bash
systemctl enable --now autofs
systemctl restart autofs
systemctl reboot
```

```bash
############################################################
# CHAPTER 14 : ACCESSING NETWORK-ATTACHED STORAGE
# Page : 521
############################################################
```

## CONTROLING ACCESS TO FILE WITH ACL
```bash
## Example 
## Copy /etc/fstab to /var/tmp name admin, the natasha could read, write and modify it, while harry without any permission.
cp /etc/fstab /var/tmp/
ls -lah /var/tmp/fstab

# change permission file fstab to group admin
chown :admin /var/tmp/fstab

# check acl file fstab
getfacl /var/tmp/fstab 
############### OUTPUT ###################
## file: var/tmp/fstab
## owner: root
## group: admin
#user::rw-
#group::r--
#other::r--
############### OUTPUT ###################

setfacl -m u:natasha:rwx /var/tmp/fstab
setfacl -m u:harry:--- /var/tmp/fstab

############### OUTPUT ###################
## file: var/tmp/fstab
## owner: root
## group: admin
#user::rw-
#user:natasha:rwx
#user:harry:---
#group::r--
#mask::rwx
#other::r--
############### OUTPUT ###################

ls  -lah /var/tmp/fstab
```

```bash
## Example2
# user contractor1, contractor2, contractor3, member group contractor.
# set acl permission to group contractors to file /home/contractors/filecon.
# ser acl read to user contractor2

cat /etc/group | grep contractors
chown -R :contractors /home/contractors/
getfacl /home/contractors
############### OUTPUT ###################
## file: home/contractors
## owner: root
## group: contractors
#user::rwx
#group::r-x
#other::r-x
############### OUTPUT ###################


getfacl /home/contractors/filecon
############### OUTPUT ###################
## file: home/contractors/filecon
## owner: root
## group: contractors
#user::rw-
#group::r--
#other::r--
############### OUTPUT ###################

setfacl  g:contractors:rwx /home/contractors/filecon
getfacl /home/contractors/filecon
############### OUTPUT ###################
## file: home/contractors/filecon
## owner: root
## group: contractors
#user::rw-
#group::r--
#group:contractors:rwx
#mask::rwx
#other::r--
############### OUTPUT ###################


setfacl -m u:contractor2:r-- /home/contractors/filecon
getfacl /home/contractors/filecon
############### OUTPUT ###################
## file: home/contractors/filecon
## owner: root
## group: contractors
#user::rw-
#user:contractor2:r--
#group::r--
#group:contractors:rwx
#mask::rwx
#other::r--
############### OUTPUT ###################

# verifikasi
su - contractor1
echo "file from contractor1" >> /home/contractors/filecon
exit

su - contractor3
echo "file from contractor3" >> /home/contractors/filecon
exit

su - contractor2
echo "file from contractor2" >> /home/contractors/filecon
############### OUTPUT ###################
#-bash: /home/contractors/filecon: Permission denied
############### OUTPUT ###################
cat /home/contractors/filecon
exit

```

# Scheduling Future Tasks
## Cron Job
```bash
#example 
# Configure a task:  run echo "hello" >> /home/natasha/file_cron command every 2 minutes in user natasha.

su - natasha
crontab -e
    ...
    */2 * * * * bash echo "hello" >> /home/natasha/file_cron
    ...
crontab -l

############################################################
# CHAPTER 13 : SCHEDULING FUTURE TASKS
# Page : 510
############################################################
```

## Manage SELinux security
```bash
#example
# file /var/www/html/index.html cannot access from httpd service
# httpd installed and use port 80

curl http://localhost/index.html
############### OUTPUT ###################
#<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
#<html>
#<head>
#<title>403 Forbidden</title>
#</head>
#<body>
#    <h1>Forbidden</h1>
#    <p>You dont have permission to access /index.html
#    on this server.<br />
#    </p>
#</body>
#</html>
############### OUTPUT ###################


# cek fcontext file /var/www/html/index.html
ls -lahZ /var/www/html/index.html
############### OUTPUT ###################
# -rw-r--r--. 1 root root unconfined_u:object_r:admin_home_t:s0 12 Sep 19 16:59 /var/www/html/index.html
############### OUTPUT ###################

# set fcontext file /var/www/html/index.html to httpd_sys_content_t
semanage fcontext -a -t httpd_sys_content_t /var/www/html/index.html

# make it persistent
restorecon -RFvv /var/www/html/index.html
ls -lahZ /var/www/html/index.html
############### OUTPUT ###################
# -rw-r--r--. 1 root root system_u:object_r:httpd_sys_content_t:s0 12 Sep 19 16:59 /var/www/html/index.html
############### OUTPUT ###################

# verifikasis
curl http://localhost/index.html
############### OUTPUT ###################
# hello world
############### OUTPUT ###################


```
```bash
#example2
# change directory web httpd to /home/web 
# httpd installed and use port 80

mkdir /home/web
echo "hello world from directory /home/web" >> /home/web/index.html

# set /home/web default web directory in file /etc/httpd/conf
vim /etc/httpd/conf/httpd.conf

# change DocumentRoot "/var/www/html" to DocumentRoot "/home/web"
# change <Directory "/var/www/html"> to  <Directory "/home/web">

# restart httpd
systemctl restart httpd

# testing access httpd 
curl http://localhost/index.html
############### OUTPUT ###################
#<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
#<html>
#<head>
#<title>403 Forbidden</title>
#</head>
#<body>
#    <h1>Forbidden</h1>
#    <p>You dont have permission to access /index.html
#    on this server.<br />
#    </p>
#</body>
#</html>
############### OUTPUT ###################


# set default fcontext directory to httpd_sys_content_t
semanage fcontext -a -t httpd_sys_content_t '/home/web(/.*)?'

# make it persistent
restorecon -RFvv /home/web/
############### OUTPUT ###################
Relabeled /home/web from unconfined_u:object_r:user_home_dir_t:s0 to system_u:object_r:httpd_sys_content_t:s0
Relabeled /home/web/index.html from unconfined_u:object_r:user_home_t:s0 to system_u:object_r:httpd_sys_content_t:s0
############### OUTPUT ###################


# verifikasis
curl http://localhost/index.html
############### OUTPUT ###################
# hello world from directory /home/web
############### OUTPUT ###################

```

## Manage network security
```bash

# allow access httpd service from outside
# ex : httpd server ip/hostname  servera

# testing access from other host
telnet 10.10.10.100 80 
############### OUTPUT ###################
# Trying servera...
# telnet: Unable to connect to remote host: No route to host
############### OUTPUT ###################
# curl http://servera/
# curl: (7) Failed to connect to servera port 80: No route to host
############### OUTPUT ###################

# check status httpd service on servera
systemctl enable --now httpd
systemctl status httpd
systemctl is-active httpd

# check port 80 open from  httpd service
netstat -nlptu | grep 80
############### OUTPUT ###################
# tcp6       0      0 :::80                   :::*                    LISTEN      10346/httpd
############### OUTPUT ###################

# problem : zones default (public) firewalld dont allow httpd service from outsite


# check firewalld active zones
firewall-cmd --get-active-zones 
############### OUTPUT ###################
# ...
# public
# ...
############### OUTPUT ###################


firewall-cmd --get-default-zone 
############### OUTPUT ###################
# public
############### OUTPUT ###################

# check allow services public zones
firewall-cmd --list-services --zone=public
############### OUTPUT ###################
# cockpit dhcpv6-client ssh
############### OUTPUT ###################

# add httpd service to firewalld public zone
firewall-cmd --add-service=http --zone=public --permanent
firewall-cmd --reload

# check allow services public zones
firewall-cmd --list-services --zone=public
############### OUTPUT ###################
# ccockpit dhcpv6-client http ssh
############### OUTPUT ###################


# check access from other server
curl servera
############### OUTPUT ###################
# hello world from directory /home/web
############### OUTPUT ###################

```

```bash
# example
# change httpd port from 80 t0 82

# edit file /etc/httpd/conf/httpd.conf 
# change Listen 80 to Listen 82
vim /etc/httpd/conf/httpd.conf 

# restart httpd service
systemctl restart httpd
############### OUTPUT ###################
# Job for httpd.service failed because the control process exited with error code.
# See "systemctl status httpd.service" and "journalctl -xe" for details.
############### OUTPUT ###################

# check error log
journalctl -xe | grep SELinux
############### OUTPUT ###################
# ...
# SELinux is preventing /usr/sbin/httpd from name_bind access on the tcp_socket port 82.
# ...
############### OUTPUT ###################

journalctl -xe | grep sealert
############### OUTPUT ###################
# ...
# sealert -l 7a3be703-e61b-4ca1-856b-14ffa815a901.
# ...
############### OUTPUT ###################

# check sealert
sealert -l 7a3be703-e61b-4ca1-856b-14ffa815a901


# problem : SELinux not allowing port 82 on httpd service

# check selinux http service port
semanage port -l | grep http
############### OUTPUT ###################
# http_cache_port_t              tcp      8080, 8118, 8123, 10001-10010
# http_cache_port_t              udp      3130
# http_port_t                    tcp      80, 81, 443, 488, 8008, 8009, 8443, 9000
# pegasus_http_port_t            tcp      5988
# pegasus_https_port_t           tcp      5989
############### OUTPUT ###################

# allow port 82 to selinux http_port_t
semanage port -a -t http_port_t -p tcp 82

# check selinux http service port
semanage port -l | grep http
############### OUTPUT ###################
# http_cache_port_t              tcp      8080, 8118, 8123, 10001-10010
# http_cache_port_t              udp      3130
# http_port_t                    tcp      82, 80, 81, 443, 488, 8008, 8009, 8443, 9000
# pegasus_http_port_t            tcp      5988
# pegasus_https_port_t           tcp      5989
############### OUTPUT ###################

# start httpd service
systemctl start httpd
systemctl is-active httpd

# testing from localhost
curl http://localhost:82
############### OUTPUT ###################
# hello world from directory /home/web
############### OUTPUT ###################

# testing from other host
curl http://servera:82
############### OUTPUT ###################
# curl: (7) Failed to connect to servera port 82: No route to host
############### OUTPUT ###################

# problem port 82 not allow on firewalld zone public

# check firewalld zone public
firewall-cmd --list-ports --zone=public

# allow port 82 tcp on firewalld zone public
firewall-cmd --add-port=82/tcp   --zone=public --permanent
firewall-cmd --reload


# check firewalld zone public
firewall-cmd --list-ports --zone=public 
############### OUTPUT ###################
# 82/tcp
############### OUTPUT ###################

# testing from other host
curl http://servera:82
############### OUTPUT ###################
# hello world from directory /home/web
############### OUTPUT ###################

```




## Understand and use essential tools 
### Use input-output redirection (>, >>, |, 2>, etc.)  
```bash
## stdin = 0
## stdout = 1
## stderr = 2

# redirect stdout to file
ls -lah ~ > normal_output.txt

# append stdout to file
ls -lah ~ >> normal_output.txt

# redirect stderr to file
find / -name "*.conf" 2> error_output.txt

# redirect stderr & stdout to file
find / -name "*.conf" > error_and_normal_output.txt2>&1
find / -name "*.conf" &> error_and_normal_output.txt

# append stderr & stdout to file
find / -name "*.conf" >> error_and_normal_output.txt2>&1
find / -name "*.conf" &>> error_and_normal_output.txt
```

### Create and edit text files
### Create hard and soft links
```bash
touch fileA.txt
ln fileA.txt fileA-hard-link.txt
ln -s fileA.txt  fileA-soft-link.txt
ls -lahi 
echo "halo " >> fileA.txt
cat fileA.txt 
cat fileA-hard-link.txt
```
